variable "instance_name" {
  description = "Valeur du Name tag pour l'instance EC2"
  type        = string
  default     = "ExampleAppServerInstance"
}
