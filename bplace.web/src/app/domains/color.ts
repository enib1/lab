export enum Color {
    White = "White",
    Black = "Black",
    Red = "Red",
    Blue = "Blue",
    Orange = "Orange",
    Yellow = "Yellow"
}
